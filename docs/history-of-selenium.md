2004년 Thoughtworks의 엔지니어 Jason Huggins이 시작하였다. 테스트를 하는 동안, 그는 [수동 테스트](https://www.lambdatest.com/learning-hub/manual-testing)가 비효율적이라는 것을 깨달았다. 그래서 그는 반복적인 작업을 자동화하기 위해 자바스크립트를 사용하는 프로그램을 만들었다. Selenium Core는 다양한 웹 애플리케이션을 자동화하는 데 사용되는 오픈 소스 프로그램이다.

### Selenium: 오픈 소스 도구
Thoughtworks 직원들만 내부적으로 Selenium을 사용하던 시절이 있었다. 그러나 2004년 말, Selenium은 오픈 소스 도구가 되었고 테스트 커뮤니티에서 사용할 수 있게 되었다. 그것이 오픈 소스가 된 정확한 날짜와 시간을 알 수 없지만, 적어도 2004년 11월이었다고 해도 무방하다. 그 기간 동안 Selenium에는 버그가 있었다.

게다가, 많은 회사들은 비효율적인 waterfall 관행을 고수하고 있었다. Selenium은 사용자들에게 버그 수정을 할 수 있는 범위를 좁혔다. Selenium이 오픈소스가 된 것은 엄청난 일이었다. 그 도구는 무료로 사용할 수 있었을 뿐만 아니라, 커뮤니티는 그 기능을 확장하기 위한 Selenium 프로젝트에 기여할 수 있었다.

### Selenium의 진화
1년 만에 Selenium의 인기가 올라가기 시작했다. 2005년 10월 Selenium은 크게 진화했다. 개발자들은 오픈 소스 도구를 위한 큰 계획을 세우기 시작했다. 원래 임무는 웹 테스트를 위한 도구로 Selenium을 사용하는 것이었다. 그러나 그것이 진화하면서, 개발자들은 Selenium을 설립자들이 원래 의도했던 것을 넘어서는 기능 추가를 상상하기 시작했다.

2007년, Jason Huggins는 Google로 옮기며 이 도구에 대한 작업을 계속했다. 새로운 기능으로 교차 플랫폼 테스트와 프레임 애플리케이션 테스트가 포함되었다. 그러나 Google로 옮긴 Huggins는 Thoughtworks를 배제하지 않았다.

### Selenium WebDriver의 연혁
2007년 Simon Stewart는 WebDriver를 개발했다. 그 또한 Thoughtworks에서 일했었다. 이 도구의 목적은 브라우저를 제어하는 것이었다. 또한 이 테스트 프레임워크는 여러 플랫폼에서 테스트할 수 있다. Selenium은 몇 가지 기능이 부족했고 WebDriver는 그것들에 대한 욕구를 촉구했다. 사실 한동안 Selenium의 경쟁자로 떠올랐다. 그러나 2011년 7월 두 도구가 Selenium WebDriver를 구성하며 경쟁은 종료되었다. 오늘날 우리에게 익숙한 WebDriver API로 합쳐졌다. 새로운 기능과 함께 Selenium의 원래 기능을 지원할 수 있었다.

### Selenium Grid의 연혁
Patrick Lightbody는 테스트 실행 시간을 최소화하기 위해 Selenium Grid를 개발했다. Selenium WebDriver가 출시된 후 Selenium 서버에는 Grid 기능이 내장되어, Selenium 명령을 여러 기계에 동시에 전송하는 것을 용이하게 했다. 중요한 단계에서 Selenium Grid는 브라우저 스크린샷을 찍을 수도 있었다.

### Selenium Remote Control의 연혁
도메인이 실행된 위치와 다를 경우, 자바스크립트 코드는 도메인에서 요소에 접근할 수 없다. 이것은 Selenium Core를 사용하는 테스터들이 웹 서버와 테스트 대상 애플리케이션을 로컬 PC에 설치하도록 강요한 것과 동일한 태생적 정책이다.

아이디어를 내고 HTTP 프록시 역할을 할 수 있는 서버를 구현한 Paul Hammant(Thoughtworks의 엔지니어) 덕분이다. 트릭은 브라우저가 테스트 중인 애플리케이션과 Selenium Core가 동일한 도메인에서 시작된다고 믿게 만드는 것이었다.

### Selenium IDE의 연혁
Shinya Kasatani는 테스트 사례의 생성을 가속화하기 위해 Selenium IDE를 개발했다. 직관적인 인터페이스, 빠른 설정과 결과는 IDE가 제공하는 주요 이점 중 일부이다. 스크립트 언어에 대한 지식이 제한되어 있더라도 포인트 앤 클릭(ppint-and-click) 인터페이스를 사용하여 테스트를 만들 수 있다.

### 마치며, 질문?
우리가 이미 알고 있듯이, 현대의 [소프트웨어 테스트 라이프 사이클](https://www.lambdatest.com/blog/software-testing-life-cycle/) 방법론은 더 짧은 릴리스 시간을 요구한다. 그러나 릴리스에 버그가 없고 원활하게 작동하려면 결정론적이고 반복 가능한 시험이 필수적이다. Selenium은 기업이 강력한 웹 애플리케이션을 보다 신속하게 제공할 수 있도록 지원해 왔으며 IT 업계에서 사용되는 가장 중요한 도구 중 하나로 자리를 잡았습니다. 전반적으로, 테스트 절차를 간소화하고 싶다면, Selenium이 최선이다!

우리는 당신이 Selenium 테스트가 무엇인지에 대해 상당히 잘 이해하기를 바란다. 궁금한 점이 있으면 언제든지 공유해주세요. 행복한 테스트!
