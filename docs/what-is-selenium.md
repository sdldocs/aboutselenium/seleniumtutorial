Selenium은 가장 유명한 오픈 소스 [테스트 자동화](https://www.lambdatest.com/automation-testing) 프레임워크 중 하나이다. Selenium은 다른 여러 브라우저나 운영 체제에서 웹 앱이나 웹 사이트에 대한 테스트를 자동화할 수 있다. Selenium은  Java, JavaScript, Python, C# 등 여러 프로그래밍 언어와의 호환성을 제공하여 테스터들이 사용하기 편한 프로그래밍 언어로 [웹사이트 테스트](https://www.lambdatest.com/web-testing)를 자동화할 수 있다.

테스터는 Selenium 프레임워크를 사용하여 반복되는 테스트 사례를 자동화하여 [테스트 주기를 더 짧게](https://www.lambdatest.com/blog/10-ways-to-speed-up-your-test-cycles/) 단축할 수 있다. CI/CD 파이프라인과 통합된 Selenium은 버그를 줄인 견고한 릴리스 배포 파이프라인을 지원할 수 있다.

## Selenium WebDriver
[Selenium WebDriver](https://www.lambdatest.com/blog/selenium-webdriver-tutorial-with-examples/)를 사용하여 자동화 테스트 스크립트를 통해 브라우저와 직접 상호 작용할 수 있다. Java, PHP, C#, Python, Ruby, Perl과 Javascript를 지원한다. Selenium WebDriver가 지원하는 브라우저는 Mozilla Firefox, Google Chrome 버전 12.0.712.0 이상, Internet Explorer, Safari, Opera 버전 11.5 이상, HTMLUnit 버전 2.9 이상 등이다. 운영 체제의 경우 Selenium WebDriver는 Windows, Linux, Mac OS 및 Solaris를 지원한다.

## Selenium Grid
Selenium Grid는 Selenium 프로젝트의 가장 유용한 구성 요소이다. Selenium Grid는 클라이언트-서버 모델을 통해 다양한 브라우저와 OS 조합에 대한 병렬 테스트를 가능하게 한다. 여기서 서버는 상호 작용할 여러 클라이언트가 있는 허브로 알려져 있다.

Selenium Grid를 사용하면 서버를 여러 원격 시스템에 연결할 수 있으며, 이를 사용하여 여러 브라우저 + OS 구성에서 동시에 브라우저 자동화 스크립트를 실행할 수 있다.

## Selenium Grid 실행 방법
![How Selenium Grid Works](./images/how-selenium-grid-works.png)<br>
(source: https://www.lambdatest.com/selenium)

허브와 노드는 Selenium Grid의 주요한 두 구성 요소이다.

### 허브 또는 서버
WebDriver 클라이언트는 허브를 통하여 액세스를 요청할 수 있다. Selenium Grid의 허브는 HTTP를 통해 JSON Wire Protocol을 사용하여 노드로 명령을 전송하는 역할을 한다.

### 노드 또는 클라이언트
Selenium Grid에서 노드는 단순히 원격 컴퓨터에서 실행되는 Selenium WebDriver의 인스턴스이다. 노드 내부에서 발생하는 현상은 우리가 Selenium WebDriver의 구조에서 설명하는 것과 유사하다. 각 노드는 허브 또는 서버로부터 수신된 명령을 기반으로 브라우저와 상호 작용하는 원격 Selenium WebDriver의 인스턴스이다.

### Selenium Grid에서 허브와 노드가 함께 실행하는 방법
Selenium Grid에서 테스트 스크립트를 실행하면 먼저 테스트 스크립트가 허브/서버로 푸시되며, 여기서 원하는 기능 클래스가 평가되어 테스트 구성을 파악합니다. 원하는 기능 클래스는 테스트 스크립트를 실행할 브라우저, 브라우저 버전 및 운영 체제를 지정한다. 등록된 노드 중에서 허브는 원하는 기능과 일치하는 적합한 노드를 찾는다. 그런 다음 허브는 적합한 브라우저 + OS 구성을 충족하는 원격 시스템에서 스크립트를 실행하기 위해 노드로 명령을 전송한다.

Selenium Grid는 서로 다른 원격 디바이스에서 동시에 여러 테스트 스크립트를 실행할 수 있기 때문에 테스트 런타임을 크게 최소화할 수 있다.

<!---
## Selenium Grid의 주요 도전 과제
비록 Selenium Grid가 브라우저 테스트를 자동화하기 위한 훌륭한 옵션이지만 Selenium Grid 내부 인프라에는 몇몇 과제가 있다.

### 최신 및 레거시 브라우저 모음
원격 컴퓨터에서 사용할 수 있는 브라우저와 운영 체제상에서 Selenium Grid를 사용한 웹 테스트만 자동화할 수 있다. 즉, 자동화된 브라우저 테스트의 철저한 라운드를 위해 항상 업데이트하거나 더 많은 노드를 허브에 추가해야 한다. 브라우저 버전이 출시되는 속도를 고려할 때 이는 어려울 수 있다.

### 디바이스 및 운영 체제 모음
브라우저와 마찬가지로 다른 공급업체에서 출시되는 최신 장치를 추적해야 한다. 노드에 새 장치와 운영 체제를 추가하려면 상당한 비용이 필요하다.

### 하드웨어 비용
Selenium Grid에 더 많은 노드를 추가하면 병렬 테스트를 처리하는 데 더 많은 비용이 필요하다. 2개의 Selenium Grid를 병렬 처리하려면 최소 4GB의 메모리 RAM이 필요하다. 3개의 테스트를 병렬로 실행하려면 4GB 메모리 RAM을 사용하는 경우가 거의 없다. 10개의 병렬 테스트 세션을 실행하는 데 필요한 하드웨어를 상상해 보자.

### 유지보수 비용
최신 장치를 Selenium Grid에 추가한 후에도 셀레늄 그리드를 위한 사내 인프라를 관리하기 위해 전용 리소스가 필요하다.

### On-Premise 제약
허브와 노드는 동일한 네트워크에 연결되어야 한다. 즉, 회사 한 장소에 Selenium Grid를 설치한다면 다른 곳에서는 액세스할 수 없다.

> **Note**: 여러분은 이러한 도전들을 어떻게 극복할지 궁금해 할 것이다. 음, 걱정하지 마세요. 우리가 당신을 지지해요! 람다 포함테스트를 통해 데스크톱과 모바일 모두에 사용할 수 있는 3000개 이상의 실제 브라우저로 구성된 클라우드 기반 온라인 셀레늄 그리드를 제공하므로 셀레늄 그리드를 사내에서 유지 관리해야 하는 번거로움을 없앨 수 있습니다. Seenium Grid는 Seenium Project에서 지원하는 모든 테스트 자동화 프레임워크 및 프로그래밍 언어와 호환됩니다. 또 뭐가 있을까요? 또한 Jenkins, CircleCI 등과 같은 CI/CD 툴과 Jira, asana 등과 같은 프로젝트 관리 툴과의 타사 통합을 통해 팀과의 협업을 개선하고 버그가 없는 구현 파이프라인을 구축할 수 있도록 지원합니다.
--->

## Selenium IDE
Selenium Recorder로도 알려진 Selenium IDE는 2006년부터 사용되어 왔다. 테스트 스크립트를 기록하고 재생할 수 있는 통합 개발 환경을 제공한다. [Selenium IDE](https://www.lambdatest.com/blog/selenium-ide-what-is-it-why-is-it-must-for-every-qa/)는 Mozilla Firefox add-on과 Google Chrome plugin을 설치하여 사용할 수 있다.

Selenium IDE를 도입한 주된 목적은 수동 테스터에게 [회귀 테스트](https://www.lambdatest.com/learning-hub/regression-testing) 프로세스를 용이하게 하기 위함이었다. Selenium IDE를 사용하면 기록 버튼을 누르고 브라우저에서 수동으로 테스트를 실행하면 테스트 케이스가 실행될 때 기록을 중지할 수 있다. 다음에 [테스트 케이스](https://www.lambdatest.com/blog/17-lessons-i-learned-for-writing-effective-test-cases/)를 실행해야 할 때는 녹음을 재생하기만 하면 브라우저 테스트가 자동으로 수행된다.

Selenium IDE는 테스트 사례를 기록하지만 브라우저와의 모든 상호 작용에 대해 Selenium 스크립트를 자동으로 생성한다. Selenese는 Selenium의 스크립팅 언어이다. 버튼 클릭, 드롭다운에서 값 선택 등과 같은 다양한 브라우저 작업을 위한 명령을 제공한다.

> **Note**: Selenium IDE 2는 Mozilla Firefox 51 이상 버전에서 지원되지 않는다. 다행히도 새로운 버전의 Selenium IDE 3가 도입되어 테스터들이 모든 Mozilla Firefox 브라우저에서 IDE를 통해 Selenium 테스트를 실행할 수 있게 되었다. 그러나 Selenium IDE에는 두 가지 주요 단점이 있었는데, 이는 현재 최신 버전에서 해결되었다.

## 기록된 테스트 스크립트의 취약성
만약 여러분이 웹사이트의 UI를 조금이라도 변경한다면, Selenium 로케이터의 변경으로 인해 기록을 사용할 수 없게 될 것이다. 예를 들어 이전에 브라우저에서 로그인 버튼을 클릭하는 상호 작용을 기록한 적이 있고 나중에 버튼의 텍스트를 로그인으로 변경하기로 결정한 경우 버튼의 로케이터 값이 테스트로 기록되었을 때와 달라지므로 Selenium IDE 스크립트가 비활성화된다. 따라서 전체 테스트 스크립트를 다시 기록해야 할 수 있다.

그러나 새로운 Selenium IDE 3.x 버전에서는 테스트 사례를 기록하는 동안 수많은 로케이터를 캡처함으로써 이 문제를 극복했다. 이제 UI 변환으로 인해 로케이터가 변경된 경우 Selenium IDE는 다른 로케이터를 찾는다. 따라서 이와 관련하여 테스트를 기록하는 동안 존재했던 모든 로케이터가 변경된 경우에만 테스트는 실패한다.

## Selenium IDE의 이종 브라우저간 지원 부족
Selenium IDE가 2006년에 도입되었을 때 당시 Mozilla Firefox의 애드온으로만 만들어졌고 구글 크롬은 2008년에 출시되었다. 그래서 Selenium IDE가 구글 크롬 웹스토어에 추가되기까지 수년이 걸렸다. 그러나 그 당시에도 2개의 주요 브라우저에서만 테스트할 수 있었기 때문에 [크로스 브라우저 테스팅](https://www.lambdatest.com/)의 관련성이 누락되었다. 하지만 이제는 아니다!

이제 Selenium IDE 3.x 버전을 사용하면 .side 파일을 사용하여 Selenium WebDriver를 통해 [Selenium 자동화](https://www.lambdatest.com/selenium-automation) 스크립트를 실행할 수 있다. SIDE Runner라는 Selenium IDE의 명령줄 테스트 실행기가 이 .side 파일을 생성한다.

SIDE Runner는 Selenium IDE와 Selenium WebDriver 사이의 중개자 역할을 하므로 다양한 브라우저에서 Selenium IDE 테스트 스크립트를 사용하여 브라우저 자동화를 수행할 수 있다.

## 더 나은 Selenium IDE 사용 가능
이러한 이유로 Selenium IDE는 개발 초기에 견인력을 얻지 못했고 프로젝트가 적극적으로 관리되지 못했다. 그러나 지금에는 상황이 바뀌었다. Selenium IDE 프로젝트는 현재 점진적으로 관리되고 있으며 다른 두 가지 대안을 제안하기도 했다. 하나는 오픈 소스인 칸투이고 다른 하나는 클로즈드 소스인 카탈론 스튜디오이다. 셀레늄 툴 중에서 Selenium IDE는 Selenium을 사용하여 수동 테스트를 자동화 테스트로 도약할 수 있도록 도와주는 가장 쉬운 도구이다.
