![Selenium WebDriver Architecture](./images/architecture-selenium-webdriver.png)<br>
(source: https://www.lambdatest.com/selenium)

Selenium WebDriver는 4가지 주요 구성 요소로 구성되어 있다.

1. Selenium 클라이언트 라이브러리
2. HTTP 클라이언트를 통한 JSON Wire Protocol
3. 브라우저 Drivers
4. 실제 브라우저

## Selenium 클라이언트 라이브러리
Selenium을 사용하면 Selenium 클라이언트 라이브러리의 도움으로 어떠 프로그래밍 언어로도 작성된 이러한 스크립트로 브라우저 자동화를 수행할 수 있다. 이러한 클라이언트 라이브러리는 테스트 스크립트와 Selenium 사이에서 인터프리터 역할을 한다. 그들은 언어 바인딩을 통해 프로그래밍 언어로 작성된 테스트 스크립트를 Selenese로 번역한다. 이렇게 하여 Selenium 테스트 스크립트를 작성하기 위해 어떤 언어를 사용했는 지에 관계없이 Selenium은 주어진 테스를 수행할 수 있다.

Selenium 프로젝트에서 제공하는 주요하고 가장 널리 사용되는 Selenium 바인딩은 다음과 같은 용도로 사용된다.

1. Java Selenium 테스팅
2. [Python Selenium 테스팅](https://www.lambdatest.com/blog/getting-started-with-selenium-python/)
3. JavaScript Selenium 테스팅
4. C# Selenium 테스팅
5. Ruby Selenium 테스팅
6. PHP Selenium 테스팅

이 페이지에서는 우선 Python Selenium 테스팅을 먼저 다루고자 한다.

## HTTP 클라이언트를 통한 JSON Wire Protocol
셀레늄을 사용하여 [브라우저 테스트를 자동화](https://www.lambdatest.com/automated-browser-testing)할 수 있다. Selenium WebDriver를 사용하면 Google Chrome, Mozilla Firefox, Safari, Microsoft Edge, Opera 등과 같은 주요 브라우저와 직접 상호 작용하는 브라우저 자동화를 수행할 수 있다. 모든 브라우저는 자체 HTTP 서버를 갖는 브라우저 드라이버로 구성된다.

JSON Wire Protocol은 HTTP 서버를 통해 브라우저 드라이버와 통신하는 역할을 한다. Selenium 클라이언트 라이브러리에서 정보를 가져온 다음 해당 브라우저 드라이버로 전달한다.

## 브라우저 Drivers
각 브라우저에는 해당 브라우저 내에서 수행되는 작업을 제어하는 드라이버를 포함하고 있다. JSON Wire Protocol이 브라우저 드라이버에 정보를 릴레이한 후 브라우저 드라이버는 Selenium 테스트 스크립트를 자동으로 실행하도록 브라우저를 제어하고 HTTP 서버를 통해 HTTP 프로토콜로 응답을 전송한다. 다음은 주요 웹 브라우저용 브라우저 드라이버이다:

1. ChromeDriver
2. FirefoxDriver
3. OperaDriver
4. SafariDriver
5. Ruby Selenium Testing
6. EdgeDriver

## 실제 브라우저
Selenium WebDriver를 사용하여 Google Chrome, Mozilla Firefox, Safari, Opera, Microsoft Edge 등 다양한 웹 브라우저와 상호 작용할 수 있다. Selenium은 웹 애플리케이션의 브라우저 테스트를 수행하는 데 도움이 되지만 기본 모바일 앱에는 적용할 수 없다. 이를 위해 [Appium](https://www.lambdatest.com/appium)이라는 또 다른 오픈 소스 프레임워크가 있다.

2012년, Simon Stewart(WebDriver의 원조 개발자)와 David Burns은 [WebDriver](https://www.w3.org/TR/webdriver/)를 W3C에서 인터넷 표준에 추가를 추진하였다. 오랜 협상 끝에, 마침내 WebDriver는 2019년 W3C 표준 프로토콜로 채택되었다.
