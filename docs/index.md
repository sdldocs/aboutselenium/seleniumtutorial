Selenium은 테스트 스크립트를 기반으로 브라우저를 제어하여 웹 테스트를 자동화하는 오픈 소스 소프트웨어이다.

이는 [lambdatest](https://lambdatest.com)의 공식적인 허가없이 [Selenium Tutorial: A Complete Guide on Selenium Automation Testing](https://www.lambdatest.com/selenium)를 편역한 것이다. 

- [Selenium 이란?](what-is-selenium.md)
- [Selenium Webdriver 구조](selenium-webdriver-architecture.md)
- [Selenium의 연혁](history-of-selenium.md)
- [Selenium의 중요성](importance-of-selenium.md)
- [Selenium의 단점](drawbacks-of-selenium.md)
- [Selenium 사용자](who-use-selenium.md)
- [Selenium WebDriver를 이용한 테스트 자동화](test-automation-with-selenium-webdriver.md)
- [시작](getting-started.md)
- [Tutorials](tutorials.md)


## To Do
- [Selenium Python Tutorial: A Python Automation Testing Guide with examples](https://www.lambdatest.com/blog/getting-started-with-selenium-python/)
- [Selenium Python Tutorial: A Comprehensive Guide With Examples and Best Practices](https://www.lambdatest.com/learning-hub/python-tutorial)
- [Complete Selenium WebDriver Tutorial: Guide to Selenium Test Automation](https://www.lambdatest.com/blog/selenium-webdriver-tutorial-with-examples/)
