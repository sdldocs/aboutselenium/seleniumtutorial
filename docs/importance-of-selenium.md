제품 팀들은 성급한 제품 출시를 주저한다. 일반적으로 프로세스가 수동인 경우 철저한 테스트는 SDLC(Software Development Life Cycle)의 일부이다. 테스트 팀의 규모에 따라 프로세스가 며칠에서 몇 주가 걸릴 수 있다. 초과 시간을 개발에 투입할 경우 대부분의 기업은 발생하는 비효율을 감당할 수 없다. 만약 연장없이 일정에 맞추려면, Selenium 자동화 테스트가 최선이다. Selenium을 사용하는 것의 주요한 이점은 다음과 같다.

## Selenium으로 신속한 크로스 브라우저 테스트
사용자는 원하는 브라우저에서 웹 응용프로그램을 실핼할 수 있다. 그렇기 때문에 브라우저 간에 호환되는 웹 사이트를 보장하는 것이 중요하다. 이렇게 하면 사용자가 사용하는 브라우저에 관계없이 항상 원활한 UI를 경험할 수 있다. 다른 브라우저에서 웹 응용 프로그램을 테스트하는 프로세스를 크로스 브라우저 테스트라고 하며, 변경 사항이 웹 응용 프로그램에 적용될 때마다 수백 개 이상의 브라우저를 테스트해야 하기 때문에 수동 테스터에게 악몽이 될 수 있다. 그러나 Selenium을 사용하면 브라우저 테스트를 자동화할 수 있다.

Selenium WebDriver는 Mozilla Firefox, Opera, Safari, Google Chrome, Microsoft Edge 및 악명 높은 Internet Explorer에서 브라우저 자동화를 수행하는 데 도움을 줄 수 있다. 이들은 브라우저 주요 경재자들이다. 그리고 그들 모두에 대한 호환성을 보장하는 단일 오픈 소스 테스트 자동화 프레임워크외에 무엇을 더 요구할 수 있을까?

Selenium을 이용한 브라우저 테스트는 전 세계 웹 테스터들의 삶을 더 쉽게 만들어 주었다.

## Selenium의 다양한 프로그래밍 언어 지원
Selenium은 여러 프로그래밍 언어를 지원하여 모든 테스터가 테스트 자동화 프레임워크를 쉽게 파악할 수 있도록 유연성을 제공한다.

어떤 프로그래밍 언어를 Selenium WebDriver가 지원하는가?

보편적인 거의 모든 프로그래밍 언어를 통해 Selenium으로 브라우저 테스트를 수행할 수 있지만, Selenium 프로젝트는 공식적으로 Java, JavaScript, C#, Python, PHP 및 Ruby의 바인딩을 제공한다.

- [Selenium Python: Running First Script](https://www.lambdatest.com/support/docs/python-with-selenium-running-python-automation-scripts-on-lambdatest-selenium-grid/)
- Selenium Java: Running First Script
- Selenium JavaScript: Running First Script
- Selenium C#: Running First Script
- Selenium PHP: Running First Script
- Selenium Ruby: Running First Script

## 다수의 운영체제를 지원하는 Selenium
Selenium은 Windows, Linux, macOS, Solaris와 같은 여러 운영 체제를 지원한다. 이렇게 하여 브라우저 + OS 조합에서 웹 응용 프로그램을 실행하여 공격적으로 테스트할 수 있다.

## CI/CD 파이프라인에 통합 가능한 Selenium
[Continuous Integration & Continuous Delivery](https://www.lambdatest.com/blog/what-is-continuous-integration-and-continuous-delivery/)는 새 릴리스 빌드를 빠르고 자주 제공해야 한다. 이제 모든 빌드가 CI/CD 파이프라인을 통과한 후 웹 애플리케이션의 브라우저 간 호환성을 수동으로 보장하는 책임을 여러분이 맡고 있다고 상상해 보자. 대역폭 비용이 얼마나 들 것인지를 알 필요가 없다. 그렇게 하는 동안 인간의 실수 가능성을 찾기 위해. Selenium을 사용하는 더 바람직한 방법이 있다!

Selenium 툴을 브라우저 자동화에 사용하면 팀은 과정을 반복할 때까지 기다릴 필요가 없다. CI 엔진은 각 팀원에게 구성, 인프라, 코드 변경 및 오류에 대한 정보를 제공한다. 이를 통해 전개(deployemnt)의 실패를 조기에 파악할 수 있다.

Selenium 자동화는 자연스럽게 반복되는 성능, 기능 및 호환성 테스트를 지원할 수 있다. 또한 개발자에게 거의 즉각적인 피드백을 제공하여 빠른 디버깅을 용이하게 한다.

> 참고: [Build An Automated Testing Pipeline With GitLab CI/CD and Selenium Grid](https://www.lambdatest.com/blog/automated-testing-pipeline-with-gitlab-ci-cd-and-selenium/)

## 애지일 체스터의 생산성을 향상시키는 Selenium WebDriver
애자일은 전체 개발 프로젝트를 소형 모듈이나 스프린트로 나눠 제품의 지속적인 개선에 초점을 맞춘 방법론이다. 매년 봄이 지나면 릴리스 사이클을 통해 제품에 새로운 기능이 더해진다. 각 반복 후에 웹 앱 또는 웹 사이트가 완전히 작동하는지 확인하기 위해 테스트를 수행한다. 이것은 지속적인 피드백을 제공하고 초기 단계에서 버그를 제거할 수 있는 여지를 제공한다. 그러나 민첩한 테스터가 [회귀 결함](https://www.lambdatest.com/blog/why-understanding-regression-defects-is-important-for-your-next-release/)을 확인하면서 새로운 요구사항에 대한 테스트를 수행하는 것은 어려울 수 있다.

Selenium은 브라우저 기반 웹 애플리케이션을 자동화하여 민첩한 테스터가 반복되는 테스트 스크립트를 자동화하여 보다 중요한 테스트 시나리오를 작성할 수 있도록 지원한다. Selenium 프레임워크는 테스트 실행 프로세스의 속도를 높이고 전체적으로 테스트 성능을 향상수 있다.

## 웹 브라우저 테스트 자동화를 위한 가장 보편적인 테스트 프레임워크 Selenium
인터넷에는 이용할 수 있는 수많은 오픈 소스 소프트웨어가 있다. 그러나 오픈 소스 프레임워크를 프로젝트에 통합하기 전에 그 신뢰성에 대해 생각해 볼 필요가 있다. 얼마나 적극적으로  누가 관리하고 있는가?

Selenium은 가장 보편적인 오픈 소스 테스트 자동화 프레임워크 중 하나이다. Selenium 프로젝트는 적극적으로 관리되고 있으며 2004년부터 Selenium 소프트웨어를 개발하고 있다. Selenium 테스트 도구는 또한 거대한 사용자 커뮤니티를 가지고 있기 때문에 문제에 봉착된 경우에 대비할 수 있다. Selenium 커뮤니티, Selenium 튜토리얼, 블로그 및 토론 포럼의 도움을 받아 쉽게 이를 해결할 수 있다.

> 참고: [ Top Selenium Automation Testing Blogs To Look Out](https://www.lambdatest.com/blog/top-21-selenium-automation-testing-blogs-to-look-out-in-2020/)
