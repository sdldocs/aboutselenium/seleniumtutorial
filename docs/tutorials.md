## Automation Testing
- [Taking Automated Screenshots Using Selenium](https://www.lambdatest.com/blog/taking-automated-screenshots-using-selenium/)
- [What is Selenium WebDriver: Tutorial With Examples](https://www.lambdatest.com/blog/selenium-webdriver-tutorial-for-cross-browser-testing/)

## Selenium Python
- [Getting Started With Selenium Python]()
- [The Ultimate Selenium Python Cheat Sheet for Test Automation]()
- [End-To-End Tutorial For Pytest Fixtures With Examples]()
- [How To Stop Test Suite after N Test Failures in Pytest?]()
- [PyTest Tutorial – Parallel Testing With Selenium Grid]()

### Selenium Python Video Tutorial
- [Selenium Tutorial Using Python and Pytest](https://www.youtube.com/watch?v=wWJJv_cT1KY&list=PLZMWkkQEwOPlcGgDmHl8KkXKeLF83XlrP)

## Selenium Locators
- [Complete Guide To Selenium Locators In Protractor (Examples)]()
- [Locators In Selenium WebDriver With Examples]()
- [Using Name Locator In Selenium Automation Testing]()
- [Using ID Locator In Selenium Automation Testing]()
- [Using TagName Locator In Selenium]()
- [Using Class Name Locator In Selenium Automation Testing]()
- [Using Link Text & Partial Link Text In Selenium]()
- [How Selenium 4 Relative Locator Can Change The Way You Test?]()
- [How WebdriverIO Uses Selenium Locators in a Unique Way]()
- [Using CSS Selectors In Selenium Automation Testing]()
- [Guide For Using XPath In Selenium With Examples]()

## Selenium Protractor
- [How To Debug Protractor Tests for Selenium Test Automation?]()
- [How To Handle Alerts And Popups In Protractor With Selenium?]()
- [Protractor Tutorial: Handling Timeouts With Selenium]()
- [Protractor Tutorial: Handling iFrames & Frames in Selenium]()
- [Protractor Tutorial: End To End Testing For AngularJS]()

## Maven Tutorial
- [Maven Tutorial For Selenium Test Automation]()

## Selenium WebDriver
- [How To Automate Calendar Using Selenium WebDriver For Testing?]()
- [How To Handle Web Table in Selenium WebDriver?]()
- [Geolocation Testing With Selenium Using Examples]()
- [Selenium Firefox Driver: Automate Testing With Firefox Browsers]()
- [How To Use Breakpoints For Debugging In Selenium WebDriver]()
- [How To Create Data Driven Framework In Selenium]()
- [Using Page Object Model (POM) Pattern In Selenium JavaScript]()
- [How To Open IE and Edge Browsers In Selenium WebDriver Using PHP]()
- [Parallel Testing With JUnit 5 And Selenium]()
- [How To Handle Frames & iFrames In Selenium JavaScript]()
- [How To Use JavaScript Wait Function In Selenium WebDriver]()
- [How To Handle Cookies in Selenium WebDriver]()
- [How To Modify HTTP Request Headers In JAVA Using Selenium WebDriver?]()
- [How To Use Strings In JavaScript With Selenium WebDriver?]()
- [How To Take Screenshots In Selenium WebDriver Using JavaScript]()
- [How to Handle JavaScript Alert in Selenium WebDriver Using Python?]()
- [How To Scroll a Page Using Selenium WebDriver?]()
- [How to use Assert and Verify in Selenium WebDriver]()
- [How To Use JavaScriptExecutor in Selenium WebDriver?]()
- [How to perform Mouse Actions in Selenium WebDriver]()
- [How To Handle Login Pop-up In Selenium WebDriver Using Java?]()
- [How To Handle Dropdowns In Selenium WebDriver Using Python?]()
- [Tutorial On Handling Keyboard Actions In Selenium WebDriver]()
- [How To Speed Up JavaScript Testing With Selenium and WebDriverIO?]()
- [How To Find Broken Images Using Selenium WebDriver?]()
- [How To Find Broken Links Using Selenium WebDriver?]()
- [How To Get Attribute Value In Selenium WebDriver?]()
- [How To Perform Localization Testing Using Selenium WebDriver?]()
- [How To Handle Internationalization In Selenium WebDriver?]()
- [Selenium RemoteWebDriver: What Is It? How Is It Different From WebDriver?]()
- [Complete Selenium WebDriver Tutorial with Examples]()
- [Top 28 Selenium WebDriver Commands in NUnit For Test Automation]()
- [Desired Capabilities in Selenium Webdriver]()
- [Using Selenium Webdriver For Full Page Screenshots]()
- [Selenium Webdriver Java Tutorial – Guide for Beginners]()
- [Why Selenium WebDriver Should Be Your First Choice for Automation Testing]()
- [Selenium WebDriver Tutorial for Cross Browser Testing]()
- [How To Set Test Case Priority In TestNG With Selenium]()

## Selenium WebDriverIO
- [WebDriverIO Tutorial: Run Your First Automation Script]()
- [WebDriverIO Tutorial: Handling Alerts & Overlay In Selenium]()
- [Cross Browser Testing With WebDriverIO]()
- [WebDriverIO Tutorial For Handling Dropdown In Selenium]()
- [WebdriverIO Tutorial: Browser]()
- [Automated Monkey Testing with Selenium & WebDriverIO]()

## Selenium IDE
- [What is Selenium IDE? Why Is It Must For Every QA?]()
- [How To Run Selenium IDE Test Over Online Selenium Grid?]()

## References
- [https://www.selenium.dev/](https://www.selenium.dev/)
- [https://www.selenium.dev/documentation/en/](https://www.selenium.dev/documentation/en/)
- [https://en.wikipedia.org/wiki/Selenium_(software)](https://en.wikipedia.org/wiki/Selenium_(software))
- [https://github.com/SeleniumHQ/selenium](https://github.com/SeleniumHQ/selenium)
